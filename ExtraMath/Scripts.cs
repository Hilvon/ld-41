﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ExtraMathScripts{
    class SquareSolver
    {
        float D;
        public bool IsSOlvable { get { return D >= 0; } }
        float _X1, _X2;
        public float X1 { get { if (D >= 0) return _X1; else return float.NaN; } }
        public float X2 { get { if (D >= 0) return _X2; else return float.NaN; } }

        public SquareSolver(float A, float B, float C)
        {
            D = B * B - 4 * A * C;
            if (D>=0)
            {
                float SQRD = Mathf.Sqrt(D);
                _X1 = (-B + SQRD) / (2 * A);
                _X2 = (-B - SQRD) / (2 * A);
            }            
        }
    }
}
