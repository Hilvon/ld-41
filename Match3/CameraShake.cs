﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {
    public float TraumaDecay;
    public float CurrentTrauma;
    public float MaxTrauma;
    public Camera ControlledCamera;

    public float HAmp, VAmp, TAmp;
    // Use this for initialization
    void Start () {
		
	}
	
    float ModifyTrauma(float pct)
    {
        return pct * pct ;
    }
	// Update is called once per frame
	void Update () {
        float curDecay = TraumaDecay * Time.deltaTime;
        if (CurrentTrauma > curDecay) CurrentTrauma -= curDecay;
        else CurrentTrauma = 0;
        if (CurrentTrauma >= MaxTrauma) CurrentTrauma = MaxTrauma;
        float AdjustedTrauma = ModifyTrauma(CurrentTrauma / MaxTrauma);

        Vector3 offset = new Vector3(Random.Range(-HAmp, HAmp), Random.Range(-VAmp, VAmp)) * AdjustedTrauma;
        float Tilt = Random.Range(-TAmp, TAmp) * AdjustedTrauma;
        ControlledCamera.transform.localPosition = offset;
        ControlledCamera.transform.localRotation = Quaternion.Euler(0, 0, Tilt);
	}
}
