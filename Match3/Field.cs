﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Field : MonoBehaviour, MouseManager.IMouseOverHandler, MouseManager.IMouseDownHandler, MouseManager.IMouseUpHandler   {
    public static Field Instance;
    //public Camera M3Camera;
    public CameraShake Shaker;
    //int[] Field;

    HashSet<int> Excluded = new HashSet<int>();
    public int RowWidth;
    public int FieldHeight;
    public int CriticalFieldSize;
    //int GetTileContent(int Row, int Col, Dictionary<int,int> Swaps = null)
    //{
    //    if (Row < 0 || Row >= FieldHeight || Col < 0 || Col >= RowWidth) return -2;
    //    int index = Row * RowWidth + Col;
    //    if (Excluded.Contains(index)) return -2; //Tile Damaged!
    //    if (Swaps != null)
    //    {
    //        if (Swaps.ContainsKey(index)) index = Swaps[index];
    //    }
    //    return Field[index];
    //}

    enum MoveMode { Right, Left, Up, Down};

    //bool IsValidMove(int Row, int Col, MoveMode Move )
    //{
    //    if (Move == MoveMode.Left && Col == 0) return false; //(out of bounds)
    //    if (Move == MoveMode.Right && Col == RowWidth -1 ) return false; //(out of bounds)
    //    if (Move == MoveMode.Down && Row == 0) return false; //(out of bounds)
    //    if (Move == MoveMode.Up && Row == FieldHeight-1) return false; //(out of bounds)

    //    int I1 = 

    //}
    public bool DBGDamage;
    public void ApplyDamage()
    {
        int ToDrop = Tiles.Keys.OrderBy(K => { return Random.Range(0, 100); }).First();
        FieldTile F = Tiles[ToDrop];
        F.Contents.Discard();
        Destroy(F.gameObject);
        Tiles.Remove(ToDrop);
        if (Tiles.Count<CriticalFieldSize)
        {
            //Debug.Log("GameOver");
            GameManager.GameOver();
        }
    }

    Dictionary<int, FieldTile> Tiles;
    //Dictionary<Token, int> TokenIndices;
    //Dictionary<int,Token> StoredTokens;
    //public Vector3 GetTokenPosition(Token T)
    //{
    //    if (TokenIndices.ContainsKey(T))
    //    {
    //        int I = TokenIndices[T];
    //        if (Tiles.ContainsKey(I))
    //        {
    //            return Tiles[I].transform.position;
    //        }
    //    }
    //    T.Discard();
    //    return Vector3.zero;        
    //}
    public FieldTile GetTile(int index)
    {
        return Tiles.ContainsKey(index) ? Tiles[index] : null;
    }
    public FieldTile GetTile(int Row, int Col)
    {
        int I = GetIndex(Row, Col);
        return I == -2 ? null : GetTile(I);
    }
    public Token GetToken(int Row, int Col)
    {
        int I = GetIndex(Row, Col);
        return I == -2 ? null : GetToken(I);
    }
    public Token GetToken(int index)
    {
        return Tiles.ContainsKey(index) ? Tiles[index].Contents : null;
    }
    public int GetIndex(int Row, int Col)
    {
        if (Row < 0 || Row >= FieldHeight || Col < 0 || Col >= RowWidth) return -2;
        return Row * RowWidth + Col;
    }

    public void TokenInPosition(Token T)
    {
        //Check if this token in now in a match!
        ArriveChangedTiles.Enqueue(T.Position);
    }

    public FieldTile TileTemplate;

    // Use this for initialization
    public void Rehash()
    {
        foreach (FieldTile F in Tiles.Values)
        {
            if (F.Contents != null) F.Contents.Discard();
        }
        for (int j = 0; j < FieldHeight; j++)
        {
            for (int i = 0; i < RowWidth; i++)
            {
                int I = j * RowWidth + i;
                if (Tiles.ContainsKey(I))
                {
                    FieldTile newTile = Tiles[I];
                    List < TokenColor > barred = new List<TokenColor>();
                    AddIfAny(ReturnIfSame(Get2Left(j, i)), barred);
                    AddIfAny(ReturnIfSame(Get2Down(j, i)), barred);

                    newTile.SpawnToken(barred);
                }
            }
        }
    }

    void Start () {
        Instance = this;
        Token.ResetPool();
        Tiles = new Dictionary<int, FieldTile>();
        //TokenIndices = new Dictionary<Token, int>();
        //StoredTokens = new Dictionary<int, Token>();
        for (int j = 0; j < FieldHeight; j++)
        {
            for (int i = 0; i < RowWidth; i++)
            {
                FieldTile newTile = Instantiate(TileTemplate, transform);
                newTile.transform.localPosition = new Vector3(i, j, 0);
                
                int I = newTile.Index = j * RowWidth + i;
                newTile.Row = j;
                newTile.Col = i;
                //Token NT = Token.GetNew(newTile.transform.position);

                Tiles[I] = newTile;

                List<TokenColor> barred = new List<TokenColor>();
                AddIfAny(ReturnIfSame(Get2Left(j, i)), barred);
                AddIfAny(ReturnIfSame(Get2Down(j, i)), barred);

                newTile.SpawnToken(barred);
            }
        }        
    }
    void AddIfAny(TokenColor Item, List<TokenColor> list)
    {
        if (Item != null) list.Add(Item);
    }
    TokenColor ReturnIfSame(IEnumerable<FieldTile> Tiles)
    {
        HashSet<TokenColor> TCs = new HashSet<TokenColor>(Tiles.Select(T => { return T.Contents.Mode; }));
        if (TCs.Count != 1) return null;
        return TCs.First();
    }

    List<FieldTile> Get2Left (int R, int C)
    {
        List<FieldTile> res = new List<FieldTile>();
        AddIfExists(R, C - 1, res);
        AddIfExists(R, C - 2, res);
        return res;
    }

    List<FieldTile> Get2Down(int R, int C)
    {
        List<FieldTile> res = new List<FieldTile>();
        AddIfExists(R - 1, C, res);
        AddIfExists(R - 2, C, res);
        return res;
    }

    List<FieldTile> Get3Above(int R, int C)
    {
        List<FieldTile> res = new List<FieldTile>();
        AddIfExists(R + 1, C, res);
        AddIfExists(R + 1, C-1, res);
        AddIfExists(R + 1, C+1, res);
        return res;
    }

    void AddIfExists(int R, int C, List<FieldTile> list)
    {
        if (list == null) return;
        int I = GetIndex(R, C);
        if (I != -2 && Tiles.ContainsKey(I))
        {
            FieldTile T = Tiles[I];
            if (T != null) list.Add(T);
        }
    }

    //void AssignTokenTo(Token T, int I)
    //{
    //    if (TokenIndices.ContainsKey(T)) VacatedSpaces.Enqueue(TokenIndices[T]);
    //    TokenIndices[T] = I;
    //    StoredTokens[I] = T;
    //}

    HashSet<FieldTile> DestroyedSpaces = new HashSet<FieldTile>(); //The indices that got struck before the Update;
    Queue<int> VacatedSpaces = new Queue<int>();    
    Queue<FieldTile> ArriveChangedTiles = new Queue<FieldTile>();

    HashSet<int> GetScoredIndices(FieldTile T)
    {
        int I = T.Index;
        int R = I / RowWidth;
        int C = I - R * RowWidth;
        return MatchPattern.GetDestroyedTokens(R, C);
    }


    int GetAwardAmount(int Tokens)
    {
        return (int)Mathf.Lerp(Tokens, Tokens * Tokens, 0.5f);
    }

    Dictionary<TokenColor, int> ScoredTokens = new Dictionary<TokenColor, int>();
    // Update is called once per frame
    void Update () {
        if (DBGDamage)
        {
            DBGDamage = false;
            ApplyDamage();
        }
        if (Token.FallingTokens == 0)
        {
            while (ArriveChangedTiles.Count > 0)
            {
                FieldTile T = ArriveChangedTiles.Dequeue();
                HashSet<FieldTile> KillZone = T.CheckSameContent();

                DestroyedSpaces.UnionWith(KillZone);
            }


            ScoredTokens.Clear();
            foreach (FieldTile T in DestroyedSpaces)
            {
                if (T != null)
                {
                    if (T.Contents != null)
                    {
                        if (ScoredTokens.ContainsKey(T.Contents.Mode)) ScoredTokens[T.Contents.Mode]++;
                        else ScoredTokens[T.Contents.Mode] = 1;
                        T.Contents.Score();
                        VacatedSpaces.Enqueue(T.Index);
                    }
                }
            }

            foreach (TokenColor TC in ScoredTokens.Keys)
            {
                TC.Award(GetAwardAmount(ScoredTokens[TC]));
                //Debug.Log("Scored! " + TC + " tokens: " + ScoredTokens[TC]);
            }

            DestroyedSpaces.Clear();

            while (VacatedSpaces.Count > 0)
            {
                int I = VacatedSpaces.Dequeue();

                FieldTile T = GetTile(I);
                if (T != null && T.Contents == null)
                {
                    int R = I / RowWidth;
                    int C = I - R * RowWidth;

                    List<FieldTile> Above = Get3Above(R, C);
                    if (Above.Count == 0)
                    {
                        T.SpawnToken();
                    }
                    else
                    {
                        Token Tok = GetAvailableToken(Above);
                        if (Tok == null)
                        {
                            VacatedSpaces.Enqueue(I);
                        }
                        else
                        {
                            FieldTile T2 = Tok.Position;
                            T2.Contents = null;
                            VacatedSpaces.Enqueue(T2.Index);
                            T.Contents = Tok;
                        }
                    }
                }

            }
        } else
        {
          Debug.Log("Token.FallingTokens");
        }
    }

    
    Token GetAvailableToken(List<FieldTile>  Fs)
    {
        foreach (FieldTile T in Fs)
        {
            if (T.Contents != null) return T.Contents;
        }
        return null;
    }

    void MouseManager.IMouseOverHandler.OnMouseExited()
    {
        if (IsMouseDown) return;
        SwapStartTile = null;
        SwapTargetTile = null;
    }
    FieldTile SwapStartTile;
    FieldTile SwapTargetTile;
    bool IsMouseDown;

    bool IsAdjesentTile (FieldTile A, FieldTile B)
    {
        int RA = A.Index / RowWidth;
        int CA = A.Index - RA * RowWidth;


        int RB = B.Index / RowWidth;
        int CB = B.Index - RB * RowWidth;

        return (((RA == RB) && (CA==CB-1 || CA==CB+1)) || ((CA==CB) && (RA==RB-1 || RA==RB+1)));
    }



    void MouseTargetChanged(FieldTile MouseTraget)
    {
        if (IsMouseDown)
        {
            if (MouseTraget == SwapStartTile) {
                SwapTargetTile = null;
                return;
            }
            if (MouseTraget != SwapTargetTile && IsAdjesentTile(MouseTraget, SwapStartTile))
            {
                SwapTargetTile = MouseTraget;
                return;
            }
        }
        else
        {
            SwapStartTile = MouseTraget;
        }
    }

    void MouseManager.IMouseOverHandler.OnMouseEntered(GameObject MouseTraget)
    {
        FieldTile FT = MouseTraget.GetComponentInParent<FieldTile>();
        if (FT != null) MouseTargetChanged(FT);
    }

    void MouseManager.IMouseOverHandler.OnMouseMoved(GameObject MouseTraget)
    {
        FieldTile FT = MouseTraget.GetComponentInParent<FieldTile>();
        if (FT != null) MouseTargetChanged(FT);
    }

    void MouseManager.IMouseDownHandler.OnMousePressed(int BtnID)
    {
        if (SwapStartTile != null)
        {
            IsMouseDown = true;
            SwapTargetTile = null;
            Debug.Log("Swap Started");
        }
    }

    void UnionIfNotNull<T>(HashSet<T> A, HashSet<T> B)
    {
        if (A == null || B == null) return;
        A.UnionWith(B);
    }

    void MouseManager.IMouseUpHandler.OnMouseReleased(int BtnID)
    {
        if (IsMouseDown)
        {
            if (SwapStartTile != null && SwapTargetTile)
            {
                //Try swapping tile contents. If no Tokens broken - swap back.
                Debug.Log("Swap Commencing");

                Token tmp = SwapStartTile.Contents;
                SwapStartTile.Contents = SwapTargetTile.Contents;
                SwapTargetTile.Contents = tmp;

                HashSet<FieldTile> ProjectedKillzone = new HashSet<FieldTile>();
                ProjectedKillzone.UnionWith(SwapStartTile.CheckSameContent());
                ProjectedKillzone.UnionWith(SwapTargetTile.CheckSameContent());

                //HashSet<int> Scored = new HashSet<int>();
                //UnionIfNotNull(Scored, GetScoredIndices(SwapStartTile));
                //UnionIfNotNull(Scored, GetScoredIndices(SwapTargetTile));

                //if (Scored.Count>0)
                //{
                //    //DestroyedSpaces.UnionWith(Scored); //--The will be removed one they complete swapping.
                //} else
                if (ProjectedKillzone.Count == 0)
                {
                    tmp = SwapStartTile.Contents;
                    SwapStartTile.Contents = SwapTargetTile.Contents;
                    SwapTargetTile.Contents = tmp;
                    Debug.Log("Swap Invalid");
                }
            }
            else
            {
                //Debug.Log("Swap Cancelled");
            }
            IsMouseDown = false;
            SwapTargetTile = null;
        }
    }
}
