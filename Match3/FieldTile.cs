﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldTile : MonoBehaviour {

    public int Index;
    public int Row, Col;
    Token _Contents;
    public Token Contents {
        get { return _Contents; }
        set {
            if (_Contents != value)
            {
                _Contents = value;
                if (_Contents!= null) _Contents.Position = this;
            }
        }
    }
    enum LineDirection { Up,Down,Left,Right}
    FieldTile GetRelativeTile(LineDirection Dir, int i)
    {
        switch (Dir)
        {
            case LineDirection.Up: return Field.Instance.GetTile(Row + i, Col);
            case LineDirection.Down: return Field.Instance.GetTile(Row - i, Col);
            case LineDirection.Left: return Field.Instance.GetTile(Row, Col - i);
            case LineDirection.Right: return Field.Instance.GetTile(Row, Col + i);
        }
        return this;
    }
    LineDirection Rev(LineDirection Dir)
    {
        switch (Dir)
        {
            case LineDirection.Up: return LineDirection.Down;
            case LineDirection.Down: return LineDirection.Up;
            case LineDirection.Left: return LineDirection.Right;
            case LineDirection.Right: return LineDirection.Left;                
        }
        return LineDirection.Up;
    }
    List<FieldTile> CheckLine(LineDirection Dir)
    {
        List<FieldTile> res = new List<FieldTile>() { this };
        TokenColor C = Contents.Mode;
        FieldTile tmp;
        int i = 1;
        bool GoPos = true, GoNeg = true;
        while (GoPos || GoNeg)
        {
            if (GoPos)
            {
                tmp = GetRelativeTile(Dir, i);
                if (tmp != null && tmp.Contents != null && tmp.Contents.Mode == C)
                {
                    res.Add(tmp);
                }
                else
                {
                    GoPos = false;
                }
            }

            if (GoNeg)
            {
                tmp = GetRelativeTile(Rev(Dir), i);
                if (tmp != null && tmp.Contents != null && tmp.Contents.Mode == C)
                {
                    res.Add(tmp);
                }
                else
                {
                    GoNeg = false;
                }
            }
            i++;
        }

        return res;
    }

    public HashSet<FieldTile> CheckSameContent()
    {
        HashSet<FieldTile> res = new HashSet<FieldTile>();
        if (Contents != null)
        {
            List<FieldTile> H = CheckLine(LineDirection.Left);
            if (H.Count >= 3) res.UnionWith(H);
            H = CheckLine(LineDirection.Up);
            if (H.Count >= 3) res.UnionWith(H);
        }
        return res;        
    }

    public void SpawnToken(List<TokenColor> barred = null)
    {
        Contents = Token.GetNew();
        Contents.Spawn(TokenColor.PickRandom(barred), transform.position + Vector3.up * 0.5f);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
