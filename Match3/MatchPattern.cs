﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu (menuName ="Match3/MatchPattern")]
public class MatchPattern : ScriptableObject {
    /// <summary>
    ///  This is a ScriptableObject that handles a pattern that can be matched.
    ///  Can check if a token in specific tile is part of the Pattern.
    ///  If Yes - reports a list of tokens that are involved in the pattern to be eliminated (scored).
    ///  And if neccesary, creates a special token.
    /// </summary>
    /// 

    


    static MatchPattern[] SortedPAtterns;
    public static HashSet<int> GetDestroyedTokens (int Row, int Col)
    {
        if (SortedPAtterns == null)
        {
            List<MatchPattern> tmp = Resources.LoadAll<MatchPattern>("").ToList();
            tmp.Sort(MainComparer);
            SortedPAtterns = tmp.ToArray();
        }
        foreach (MatchPattern MP in SortedPAtterns)
        {
            Debug.Log(MP.name);
            HashSet<int> matched = MP.CheckMatch(Row, Col);
            if (matched != null)
            {
                //Debug.Log("MatchFound! " + MP.name);
                return matched;
            }
        }
        return null;
    }

    public Sprite Shape;
    public Sprite Destruction;

    IntPair[] Targets;
    IntPair[] DestructionPattern;
    bool isHSymmetry;

    public int Priority;
    
    Color[] getPixelsFromSprite(Sprite S)
    {
        Rect R = S.textureRect;
        return S.texture.GetPixels((int)R.xMin, (int)R.yMin, (int)R.width, (int)R.height);
    }


    HashSet<int> CheckMatch(int Row, int Col)
    {
        if (Targets==null || Targets.Length ==0)
        {
            Debug.Log(name + ": Initializing from Texture");
            isHSymmetry = true;
            Rect TR = Shape.rect;
            int H = (int)TR.height;
            int W = (int)TR.width;

            Color[] Pixels = Shape.texture.GetPixels((int)TR.xMin, (int)TR.yMin, W, H);
            List<IntPair> Pairs = new List<IntPair>();
            for (int i = 0; i < TR.height; i++)
            {
                for (int j = 0; j < TR.width; j++)
                {
                    isHSymmetry = isHSymmetry && (Pixels[i * W + j] == Pixels[i * W + W - j - 1]);
                    if (Pixels[i * ((int)TR.width) + j] == Color.black) Pairs.Add(new IntPair() { X = j, Y = i });
                }                
            }
            Targets = Pairs.ToArray();
            Debug.Log(name + ": "+ Targets.Length+" points located");

            if (Destruction != null) {
                TR = Destruction.rect;
                H = (int)TR.height;
                W = (int)TR.width;
                IntPair P = new IntPair() { Y = (H + 1) / 2, X = (W + 1) / 2 }.Invese();
                Pixels = Destruction.texture.GetPixels((int)TR.xMin, (int)TR.yMin, W, H);

                Pairs = new List<IntPair>();
                for (int i = 0; i < TR.height; i++)
                {
                    for (int j = 0; j < TR.width; j++)
                    {
                        if (Pixels[i * ((int)TR.width) + j] == Color.black) Pairs.Add((new IntPair() { X = j, Y = i }).Offset(P));
                    }
                }
                DestructionPattern = Pairs.ToArray();
            }
        }

        //foreach (Color C in getPixelsFromSprite(Shape)) {
        //    Debug.Log(C);
        //}
        //Debug.Log(Row + ", " + Col + "; " + Field.Instance + "; " + Field.Instance.GetToken(Row, Col));
        TokenColor TC = Field.Instance.GetToken(Row, Col).Mode;
        IntPair FC = new IntPair() { X = Col, Y = Row };


        foreach (IntPair IP in Targets)
        {
            HashSet<int> res = CheckAllRotations(TC, FC, IP, false);
            if (res != null) return res;
            if (!isHSymmetry) res = CheckAllRotations(TC, FC, IP, true);
            if (res != null) return res;
        }
        return null;
    }

    HashSet<int> CheckAllRotations(TokenColor TC, IntPair FC, IntPair IP, bool isFlipped)
    {
        for (int i = 0; i < 4; i++)
        {
            if (CheckMatchBy(FC, IP.Invese(), TC, i, isFlipped))
            {
                Debug.Log("We have a match with " + name + " at position " + FC.X + "x" + FC.Y);
                HashSet<int> res = new HashSet<int>();
                res.UnionWith(ListMatchedTokens(FC, IP.Invese(),i,isFlipped));
                if (DestructionPattern != null && DestructionPattern.Length > 0) res.UnionWith(ListCollateralTokens(FC,i));
                return res;
            }
        }
        return null;
    }

    List<int> ListMatchedTokens(IntPair FieldPos, IntPair IRootPos, int Rotation, bool Flipped)
    {
        List<int> res = new List<int>();
        foreach (IntPair IP in Targets)
        {
            IntPair newFieldPos = FieldPos.Offset(IP.Offset(IRootPos).Flipped(Flipped).Rotated(Rotation));
            res.Add(Field.Instance.GetIndex(newFieldPos.Y, newFieldPos.X));
        }
        return res;
    }
    List<int> ListCollateralTokens(IntPair FieldRoot, int Rotation)
    {
        List<int> res = new List<int>();
        foreach (IntPair IP in DestructionPattern) 
        {
            IntPair newFieldPos = FieldRoot.Offset(IP.Rotated(Rotation));
            int T = Field.Instance.GetIndex(newFieldPos.Y, newFieldPos.X);
            if (T != -2) res.Add(T);
        }
        return res;
    }

    bool CheckMatchBy(IntPair FieldPos, IntPair IRootPos, TokenColor C, int Rotation, bool Flipped)
    {
        foreach (IntPair IP in Targets)
        {
            IntPair RelativeFieldPos = FieldPos.Offset(IP.Offset(IRootPos).Flipped(Flipped).Rotated(Rotation));
            Token T = Field.Instance.GetToken(RelativeFieldPos.Y, RelativeFieldPos.X);
            if (T==null) return false;
            if (T.Mode != C) return false;
        }
        return true;
    }

    

    [System.Serializable]
    public class IntPair
    {
        public int X, Y;

        public IntPair Offset(IntPair Other)
        {
            return new IntPair() { X = X + Other.X, Y = Y + Other.Y };
        }
        public IntPair Invese()
        {
            return new IntPair() { X = -X, Y = -Y};
        }
        public bool IsSame(IntPair Other)
        {
            return X == Other.X && Y == Other.Y;
        }      
        public IntPair Flipped(bool IsFlipped)
        {
            if (IsFlipped) return new IntPair() { X = -X, Y = Y };
            else return new IntPair() { X = X, Y = Y };
        }
        public IntPair Rotated(int Rot)
        {
            switch(Rot)
            {
                case 1: return new IntPair() { X = -Y, Y = X };
                case 2: return new IntPair() { X = -X, Y = -Y };
                case 3: return new IntPair() { X = Y, Y = -X };
                default: return new IntPair() { X = X, Y = Y };
            }
        }
    }
    public class Comparer : IComparer<MatchPattern>
    {
        public int Compare(MatchPattern x, MatchPattern y)
        {
            return x.Priority > y.Priority ? 1 : x.Priority < y.Priority ? -1 : 0;
        }
    }
    static Comparer MainComparer = new Comparer();
}
