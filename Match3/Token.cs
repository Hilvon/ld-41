﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Token : MonoBehaviour {
    FieldTile _Position;
    public static int FallingTokens=0;
    
    public FieldTile Position { get { return _Position; }
    set {
            if (_Position != value)
            {
                _Position = value;
                
                if (_Position != null)
                {
                    if (!NotInPosition)
                    {
                        FallingTokens++;
                        NotInPosition = true;
                    }

                    _Position.Contents = this;
                }
            }
        }
    }
    
    // Use this for initialization
	void Start () {
		
	}
    // Update is called once per frame
    bool NotInPosition;

    void Update () {
        if (NotInPosition)
        {
            Vector3 newPos = _Position.transform.position;
            if (Vector3.Distance(newPos, transform.position) <= .1f)
            {
                transform.position = newPos;
                NotInPosition = false;
                FallingTokens--;
                Field.Instance.TokenInPosition(this);
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, newPos, Time.deltaTime * 6);//  Vector3.Lerp(transform.position, newPos, Time.deltaTime * 5);
            }
        }
	}

    public void Score()
    {
        Retire();
    }

    public void Discard()
    {
        Retire();
    }

    protected virtual void Retire()
    {
        
        gameObject.SetActive(false);
        if (Position.Contents == this)
        {
            Position.Contents = null;
            //Field.Instance.
        }
        Position = null;        
        PooledItems.Push(this);
    }
    public TokenColor Mode;
    public void Spawn(TokenColor Mode, Vector3 Position)
    {
        gameObject.SetActive(true);
        transform.position = Position;
        this.Mode = Mode;
        foreach (ITokenDetals ITD in GetComponentsInChildren<ITokenDetals>(true))
        {
            ITD.Display(Mode);
        }
    }

    public interface ITokenDetals
    {
        void Display(TokenColor C);
    }
    public static void ResetPool()
    {
        while (PooledItems.Count >0)
        {
            Token T = PooledItems.Pop();
            if (T != null) Destroy(T.gameObject);
        }        
    }
    static Token Template;
    static Stack<Token> PooledItems = new Stack<Token>();
    static Token CreateNew()
    {
        if (Template == null)
        {
            Template = Resources.LoadAll<Token>("").FirstOrDefault();
        }
        return Instantiate(Template, Field.Instance.transform);
    }
    static Token Fetch()
    {
        if (PooledItems.Count > 0) return PooledItems.Pop();
        else return CreateNew();
    }

    public static Token GetNew()
    {
        Token res = Fetch();
        return res;
    }
}
