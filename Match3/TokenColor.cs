﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Events;
[CreateAssetMenu(menuName = "Match3/TokenColor")]
public class TokenColor : ScriptableObject {
    static TokenColor[] TCs;
    public static TokenColor PickRandom(IEnumerable<TokenColor> barred = null)
    {
        if (TCs == null)
        {
            TCs = Resources.LoadAll<TokenColor>("");
        }

        return (barred == null ? TCs : TCs.Except(barred)).OrderBy((C) => { return Random.Range(0, 100); }).FirstOrDefault();

        // return TCs[Random.Range(0, TCs.Length)];
    }

    UnityAction<int> OnScored;
    public Sprite BaseSprite;

    public enum AwardType { WeaponEnergy, ShieldEnergy, Missle, ThrusterEnergy, XP }
    public AwardType MyAward;

    public void AwardWeaponCharge(int Amount)
    {
        PlayerShip.Instance.AddWeapons(Amount);
    }
    public void AwardSheldCharge(int Amount)
    {
        PlayerShip.Instance.AddShields(Amount);
    }
    public void AwardMissle(int Amount)
    {
        PlayerShip.Instance.AddMissles(Amount);
    }
    public void AwardThrusterCharge(int Amount)
    {
        PlayerShip.Instance.AddThruster(Amount);
    }
    public void AwardXP(int Amount)
    {
        PlayerShip.Instance.AddXP(Amount);
    }

    public void Award(int Amount)
    {
        switch (MyAward)
        {
            case AwardType.WeaponEnergy:
                AwardWeaponCharge(Amount);
                break;
            case AwardType.ShieldEnergy:
                AwardSheldCharge(Amount);
                break;
            case AwardType.ThrusterEnergy:
                AwardThrusterCharge(Amount);
                break;
            case AwardType.Missle:
                AwardMissle(Amount);
                break;
            default:
                AwardXP(Amount);
                break;
        }
    }


    // Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
