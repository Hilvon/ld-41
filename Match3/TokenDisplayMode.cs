﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class TokenDisplayMode : MonoBehaviour, Token.ITokenDetals {
    SpriteRenderer _SR;
    SpriteRenderer SR {
        get {
            if (_SR == null)
            {
                _SR = GetComponent<SpriteRenderer>();
            }
            return _SR;
        }
    }

    public void Display(TokenColor C)
    {
        SR.sprite = C.BaseSprite;
    }
}
