﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseManager : MonoBehaviour {
    public Camera RayCaster;


	// Use this for initialization
	void Start () {
		
	}

    IMouseOverHandler curMouseOver;
    GameObject LastTarget;
	// Update is called once per frame
	void Update () {
        GameObject newTarget = FindMouseOverObject();
        if (newTarget != LastTarget) {
            IMouseOverHandler newMouseOverHandler = newTarget == null ? null : newTarget.GetComponentInParent<IMouseOverHandler>();
            if (curMouseOver != newMouseOverHandler)
            {
                if (curMouseOver != null) curMouseOver.OnMouseExited();
                if (newMouseOverHandler != null) newMouseOverHandler.OnMouseEntered(newTarget);
                curMouseOver = newMouseOverHandler;
            }
            else
            {
                if (curMouseOver != null) curMouseOver.OnMouseMoved(newTarget);
            }
            LastTarget = newTarget;
        }

        IMouseDownHandler MDH = GetComponentInParent<IMouseDownHandler>();
        IMouseUpHandler MUH = GetComponentInParent<IMouseUpHandler>();
        IMouseBtnHandler MBH = GetComponentInParent<IMouseBtnHandler>();

        for (int i = 0; i < 3; i++)
        {
            if (MDH != null && Input.GetMouseButtonDown(i)) MDH.OnMousePressed(i);
            if (MBH != null && Input.GetMouseButton(i)) MBH.OnMouseButton(i);
            if (MUH != null && Input.GetMouseButtonUp(i)) MUH.OnMouseReleased(i);
        }
    }
    public LayerMask HitMask;
    GameObject FindMouseOverObject()
    {
        RaycastHit hitInfo;
        if (Physics.Raycast(RayCaster.ScreenPointToRay(Input.mousePosition), out hitInfo, 999, HitMask)) {
            return hitInfo.collider.gameObject;
        }
        return null;
    }

    public interface IMouseDownHandler
    {
        void OnMousePressed(int BtnID);
    }
    public interface IMouseUpHandler
    {
        void OnMouseReleased(int BtnID);
    }
    public interface IMouseBtnHandler
    {
        void OnMouseButton(int BtnID);
    }
    public interface IMouseOverHandler
    {
        void OnMouseExited();
        void OnMouseEntered(GameObject MouseTraget);
        void OnMouseMoved(GameObject MouseTraget);
    }
}
