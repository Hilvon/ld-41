﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTrackOjcect : MonoBehaviour {
    public Transform Focus;
    public float LerpIntensity;
    public Camera ControlledCamera;
	// Use this for initialization
	void Start () {
        TargetCameraZoom = CurCameraZoom = ControlledCamera.orthographicSize;        
    }

    float CurCameraZoom;
    float TargetCameraZoom;
    public float zoomspeed;
    public void ZoomTo(float Val)
    {
        TargetCameraZoom = Val;
    }
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, Focus.position, LerpIntensity*Time.deltaTime);

        ControlledCamera.orthographicSize = CurCameraZoom = Mathf.Lerp(CurCameraZoom, TargetCameraZoom, Time.unscaledDeltaTime * zoomspeed);

    }
}
