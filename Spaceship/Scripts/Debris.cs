﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour {
    public GameObject DestructionTemplate;
    public float MinLifespan;
    public float MaxLifespan;
	// Use this for initialization
	
        public float Prime()
    {
        timer = Random.Range(MinLifespan, MaxLifespan);
        RotationSpeed = Random.Range(-15f, 15f);
        drift = transform.localPosition * 0.33f;
        drift.z = 0;
        return timer;
    }
    float RotationSpeed;
    Vector3 drift;
    float timer;
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;
        if (timer<=0)
        {
            Instantiate(DestructionTemplate, transform.position, Quaternion.identity);
        } else
        {
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + RotationSpeed*Time.deltaTime);
            transform.position = transform.position + drift * Time.deltaTime;
        };

	}
}
