﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebrisGroup : MonoBehaviour {

    // Use this for initialization
    protected float MaxLS = 0;
    void Start () {
        foreach (Debris D in GetComponentsInChildren<Debris>())
        {
            float ls = D.Prime();
            if (MaxLS <= ls) MaxLS = ls;
        }
        Destroy(gameObject, MaxLS + 0.1f);
	}
	
}
