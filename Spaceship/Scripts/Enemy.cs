﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public abstract class Enemy : MonoBehaviour, WeaponsSystems.IDamagable {
    public static List<Enemy> AllEnemies = new List<Enemy>();

    static Enemy[] Prefabs;
    public static Enemy PickEnemy(float MaxCost)
    {
        if (Prefabs == null) Prefabs = Resources.LoadAll<Enemy>("");
        return Prefabs.Where((E) => { return (E.DontSpawnTill <= GameManager.GameTime) && (E.Cost <= MaxCost); }).OrderBy(Random).FirstOrDefault();
    }
    static int Random(Enemy E) { return UnityEngine.Random.Range(1, 100); }

    public float HP;
    public float Cost;
    public float DontSpawnTill;
    public float minSpawnDistance;
    public float MaxSpawnDistance;

    private void OnEnable()
    {
        AllEnemies.Add(this);
    }
    private void OnDisable()
    {
        AllEnemies.Remove(this);
    }

    public GameObject DestructionPrefab;

    public void ReceiveDamage(float Amount)
    {
        Debug.Log("Receiving damage... " + Amount);
        HP -= Amount;
        if (
            HP<=0)
        {
            Instantiate(DestructionPrefab, transform.position, transform.rotation);
            GameManager.Score += (int)Cost;
            Destroy(gameObject);
        }
    }
}
