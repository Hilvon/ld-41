﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(NavigationModule))]
public class EnemyGunship : Enemy {

	// Use this for initialization
	void Start () {
		
	}

    NavigationModule _myNM;
    public NavigationModule Navigation {
        get {
            if (_myNM == null) _myNM = GetComponent<NavigationModule>();
            return _myNM;
        }
    }

    public float ReloadTime;
    float Cooldown;

    public float MatchVelDistSqr;
    public float MaxVel;

    public Projectile Ammo;
    // Update is called once per frame
    void Update () {
        Cooldown -= Time.deltaTime;
        if (Cooldown<=0)
        {
            Cooldown += ReloadTime;
            Vector3 ShotTarget = WeaponsSystems.FindInterceptPosition(transform.position, PlayerShip.Position, PlayerShip.Velocity, Ammo.Vel);
            Projectile newP = Instantiate(Ammo, transform.position, Quaternion.LookRotation(ShotTarget-transform.position,Vector3.back));
        }

        Vector3 ToPlayer = PlayerShip.Position - transform.position;
        NavigationModule N = Navigation;

        if (ToPlayer.sqrMagnitude > MatchVelDistSqr)
        {
            float DirectCourse = WeaponsSystems.GetHeadingTowards(ToPlayer.normalized);
            N.RequiredHeading = DirectCourse;
            N.NeededSpeed = N.MaxSpeed;
        }
        else
        {
            N.Match(PlayerShip.Instance.Navigation);
        }

	}

    
}
