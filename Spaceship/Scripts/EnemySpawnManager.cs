﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnManager : MonoBehaviour {
    public int WaveNumber;
    public float NextWaveTimer;
    public float NextWaveSize;
    public float WaveFreq;
    public float SizeIncrement;
	// Use this for initialization
	void Start () {
        NextWaveDirection = Quaternion.Euler(0, 0, Random.Range(0f, 360f));

    }

    Quaternion NextWaveDirection;
	// Update is called once per frame
	void Update () {
        NextWaveTimer -= Time.deltaTime;
        if (NextWaveTimer <=0)
        {
            NextWaveTimer += WaveFreq;
            float Budget = NextWaveSize;
            NextWaveSize *= SizeIncrement;
            WaveNumber++;

            Debug.Log("Start spawning...");
            Enemy tmp = Enemy.PickEnemy(Budget);
            while (tmp != null)
            {
                Debug.Log("Spawning: "+tmp.name+" Remaining bdget: "+Budget);
                Budget -= tmp.Cost;
                Vector3 newPos = PlayerShip.Position + NextWaveDirection * Vector3.down * Random.Range(tmp.minSpawnDistance, tmp.MaxSpawnDistance) + NextWaveDirection*Vector3.left*Random.Range(-7f,7f);
                Instantiate(tmp, newPos, NextWaveDirection);
                tmp = Enemy.PickEnemy(Budget);
            }
            NextWaveDirection = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
        }

    }
}
