﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
    public float LifeSpan;
	// Use this for initialization
	void Start () {
        Destroy(gameObject, LifeSpan);
	}	
}
