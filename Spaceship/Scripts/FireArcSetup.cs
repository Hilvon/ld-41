﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireArcSetup : MonoBehaviour {
    public Material EmptyArcMaterial;
    public Material LockedArcMaterial;
    public float FireCooldown;
    public float EnergyPerShot;
    public float DamagePerShot;
}
