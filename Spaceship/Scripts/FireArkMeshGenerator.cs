﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class FireArkMeshGenerator : MonoBehaviour {
    // Use this for initialization
    public LaserBeam LaserPrefab;
    MeshRenderer _myMR;
	void Start () {
        _myMR = GetComponent<MeshRenderer>();
        RefreshTimer = Random.Range(0, RefreshFreq);
    }

    FireArcSetup _FAS;
    FireArcSetup FAS { get { if (_FAS == null) _FAS = GetComponentInParent<FireArcSetup>(); return _FAS; } }
    
    public float Arc;
    public float Dist;

    public float scanFreq;

    float PrevArc=0, PrevDist =0;
    float dotThreshold;
    Enemy TargetEnemy;

    float Cooldown;
    public float RefreshFreq;
    float RefreshTimer;
	// Update is called once per frame
	void Update () {
        RefreshTimer -= Time.deltaTime;
        Cooldown -= Time.deltaTime;
        if (RefreshTimer <=0)
        {
            RefreshTimer += RefreshFreq;
            float Fill = Cooldown <= 0 ? 1 : 1 - Cooldown / FAS.FireCooldown;
            //Debug.Log(name + " Fill: " + Fill);

            GenerateArc(Arc, Dist, Fill);

            if (Arc != PrevArc)
            {
                dotThreshold = Mathf.Cos(Arc * Mathf.PI / 360);
                PrevArc = Arc;
                //Debug.Log("Dot threshold: " + dotThreshold);
            }

            if (PlayerShip.Instance.Target != null && IsInArc(PlayerShip.Instance.Target))
            {
                TargetEnemy = PlayerShip.Instance.Target.GetComponent<Enemy>();
                Attack();
            } else
            {
                TargetEnemy = null;
            }

            if (TargetEnemy == null)
            {
                TargetEnemy = Enemy.AllEnemies.Where((E) =>
                    {
                        return IsInArc(E.transform);
                    }).FirstOrDefault();
            }

            //Debug.Log(name + " DETECTED: " + TargetEnemy);
            SetTopMaterial((TargetEnemy != null) ?  FAS.LockedArcMaterial : FAS.EmptyArcMaterial);
        }
    }

    bool IsInArc(Transform T)
    {
        Vector3 toE = T.position - transform.position;
        if (toE.sqrMagnitude > Dist * Dist) return false;
        return Vector3.Dot(toE.normalized, transform.up) >= dotThreshold;
    }

    Mesh localMesh;
    public void GenerateArc(float angle, float distance, float Fill)
    {
        if (localMesh == null)
        {
            GetComponent<MeshFilter>().sharedMesh = localMesh = GenerateMesh(angle, distance, Fill);
        } 
        else
        {
            UpdateMesh(localMesh, angle, distance,  Fill);
        }
        
    }

    public void Attack()
    {
        if (TargetEnemy == null) return; //No target!
        if (Cooldown >= 0) return; //Still on cooldown!
        if (PlayerShip.Instance.WeaponEnergy < FAS.EnergyPerShot) return; //Not enough power
        Cooldown = FAS.FireCooldown;
        LaserBeam L = Instantiate(LaserPrefab);
        L.ShootAtTarget(transform.position, TargetEnemy.transform.position);
        TargetEnemy.ReceiveDamage(FAS.DamagePerShot);
        PlayerShip.Instance.WeaponEnergy -= FAS.EnergyPerShot;
    }

    void UpdateMesh(Mesh M, float angle, float distance, float Fill)
    {
        Vector3[] newVerst = M.vertices;
        FillVerticesArray(newVerst, angle * Mathf.PI / 360, distance, Fill);
        M.vertices = newVerst;

        M.RecalculateBounds();
        M.RecalculateNormals();
        M.RecalculateTangents();
    }

    Material[] materials;
    void SetTopMaterial(Material M)
    {
        if (materials == null)
        {
            materials = new Material[2];
            materials[0] = FAS.EmptyArcMaterial;
        }
        materials[1] = M;
        _myMR.sharedMaterials = materials;
    }

    Mesh GenerateMesh(float angle, float distance, float Fill)
    {
        Mesh Res = new Mesh();
        Vector3[] newVerst = new Vector3[8];
        FillVerticesArray(newVerst, angle * Mathf.PI / 360, distance, Fill);
        Res.vertices = newVerst;
        int[] newTris = new int[6];
        newTris[0] = 0;
        newTris[1] = 1;
        newTris[2] = 2;
        newTris[3] = 1;
        newTris[4] = 3;
        newTris[5] = 2;

        Res.subMeshCount = 2;
        Res.SetTriangles(newTris,0);


        newTris[0] = 4;
        newTris[1] = 5;
        newTris[2] = 6;
        newTris[3] = 5;
        newTris[4] = 7;
        newTris[5] = 6;

        Res.SetTriangles(newTris, 1);



        Vector2[] newUVs = new Vector2[8];
        newUVs[0] = Vector2.right;
        newUVs[1] = Vector2.zero;
        newUVs[2] = Vector2.one;
        newUVs[3] = Vector2.up;
        newUVs[4] = Vector2.right;
        newUVs[5] = Vector2.zero;
        newUVs[6] = Vector2.one;
        newUVs[7] = Vector2.up;

        Res.uv = newUVs;

        Res.RecalculateBounds();
        Res.RecalculateNormals();
        Res.RecalculateTangents();

        SetTopMaterial(FAS.EmptyArcMaterial);
        return Res;
    }
    static Vector3 off = new Vector3(0, 0, 0.001f);
    void FillVerticesArray(Vector3[] V, float halfangle, float distance, float Fill)
    {
        V[0] = Vector3.zero;
        V[1] = new Vector3(-Mathf.Sin(halfangle)*distance, Mathf.Cos(halfangle)*distance, 0);
        V[2] = new Vector3(Mathf.Sin(halfangle)*distance, Mathf.Cos(halfangle)*distance, 0);
        V[3] = new Vector3(0, (2 - Mathf.Cos(halfangle)) * distance, 0);

        V[4] = Vector3.zero + off;
        V[5] = V[1] * Fill + off;
        V[6] = V[2] * Fill + off;
        V[7] = V[3] * Fill + off;

    }
}
