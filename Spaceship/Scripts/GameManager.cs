﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    // Use this for initialization
    static GameManager Main;
	void Start () {
        Main = this;        
        StartGame();
    }
    public static int Score;
    public static float GameTime;
    public static void GameOver() {
        SlowTime();
        PlayerShip.Instance.Die();
        if (Main != null) Main.ShowGameoverScreen();        
    }
    public GameOverScreen GameoverScreen;
    public GameObject GamePlayUI;
    void ShowGameoverScreen()
    {
        GameoverScreen.Score.text = ""+Score + PlayerShip.Instance.XP;
        GameoverScreen.Time.text = FormatTime(GameTime);
        GameoverScreen.gameObject.SetActive(true);
        GamePlayUI.SetActive(false);
    }
    string FormatTime(float seconds)
    {
        System.TimeSpan TS = System.TimeSpan.FromSeconds(seconds);
        if (TS.Hours > 0) return TS.Hours + ":" + DblDgt(TS.Minutes) + ":" + DblDgt(TS.Seconds);
        if (TS.Minutes > 0) return TS.Minutes + ":" + DblDgt(TS.Seconds);
        return TS.Seconds + " sec";

    }
    string DblDgt(int Num)
    {
        return (Num > 9 ? "" : "0") + Num;
    }
    void StartGame()
    {
        ResumeTime();
        GameoverScreen.gameObject.SetActive(false);
        GamePlayUI.SetActive(true);
        GameTime = 0;
    }

	// Update is called once per frame
	void Update () {
        GameTime += Time.deltaTime;
        if (curTimeSpeed != TargetTimeSpeed)
        {
            Time.timeScale = curTimeSpeed = Mathf.MoveTowards(curTimeSpeed, TargetTimeSpeed, Time.unscaledDeltaTime * 2);
        }
	}
    static float curTimeSpeed;
    static float TargetTimeSpeed;
    static void StopTime()
    {
        Time.timeScale = curTimeSpeed = TargetTimeSpeed = 0;
    }
    public static void ResumeTime()
    {
        TargetTimeSpeed = 1;
    }
    public static void SlowTime()
    {
        TargetTimeSpeed = 0.25f;
    }
    public static void SolidifyGameover(float F)
    {
        Debug.Log(F);
        if (Main!=null)
        {
            Main.GameoverScreen.GetComponent<CanvasGroup>().alpha = Mathf.Clamp01(F);
            if (F >= 0.99f) StopTime();
        }
    }
}
