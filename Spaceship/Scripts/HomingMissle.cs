﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingMissle : WeaponsSystems
{
    public Transform Target;
    public float myVel;
    // Use this for initialization
	void Start () {
    }
    public NavigationModule NM;
    // Update is called once per frame
    Vector3 prevTargetPos;
    public AnimationCurve SpeedPerAOT;

    protected override void Update()
    {
        base.Update();

        if (Target == null) Die();
        else
        {
            Vector3 TVel = (Target.position - prevTargetPos) / Time.deltaTime;
            Vector3 InterceptPosition = WeaponsSystems.FindInterceptPosition(transform.position, Target.position, TVel, myVel);
            //transform.rotation = Quaternion.Euler(0, 0, WeaponsSystems.GetHeadingTowards((InterceptPosition - transform.position).normalized));
            //transform.position = transform.position + transform.up * myVel*Time.deltaTime;

            NM.RequiredHeading = GetHeadingTowards((InterceptPosition - transform.position).normalized);
            NM.NeededSpeed = NM.MaxSpeed * SpeedPerAOT.Evaluate(NM.headingDifference / 180);
        }
    }


}
