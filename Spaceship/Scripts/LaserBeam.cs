﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class LaserBeam : MonoBehaviour {
    float LifeSpan;
    // Use this for initialization
    LineRenderer LR;
	void Start () {
        LifeSpan = WidthOverTime.keys[WidthOverTime.keys.Length - 1].time;
        LR = GetComponent<LineRenderer>();
    }
    public AnimationCurve WidthOverTime;
    float timer = 0;
	// Update is called once per frame
    public void ShootAtTarget(Vector3 From, Vector3 To)
    {
        LR = GetComponent<LineRenderer>();
        LR.SetPosition(0, From);
        LR.SetPosition(1, To);
    }
	void Update () {
        timer += Time.deltaTime;
        LR.widthMultiplier = WidthOverTime.Evaluate(timer);
        if (timer > LifeSpan) Destroy(gameObject);
    }
}
