﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleBay : MonoBehaviour {
    public float ReloadTime;
    public float MissleCost;

    float timer;

    public HomingMissle MissleTemplate;

    public void LaunchMissleAt(Transform Target)
    {
        Debug.Log("Missle firing attempt");
        if (timer > 0) return;
        if (PlayerShip.Instance.Missles < MissleCost) return;
        if (PlayerShip.Instance.Target == null) return;
        timer = ReloadTime;
        PlayerShip.Instance.Missles -= MissleCost;
        HomingMissle newMsl = Instantiate(MissleTemplate,transform.position, transform.rotation);
        newMsl.Target = Target;
        
        newMsl.NM.CloneHeadingOf(PlayerShip.Instance.Navigation);
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer -= Time.deltaTime;

    }
}
