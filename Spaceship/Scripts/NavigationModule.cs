﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationModule : MonoBehaviour {
    
    public float MaxAcceleration;
    public float MaxRotation;
    public float Maneuverability;
    public float MaxSpeed, MinSpeed;

    public float RequiredHeading;
    public float NeededSpeed;

    float curSpeed;
    float curHeading;

    public float headingDifference {
        get {
            float tmp = Mathf.Abs(RequiredHeading - curHeading);
            if (tmp > 180) tmp = 360 - tmp;
            return tmp;
        }
    }

    public Vector3 Velocity { get; private set; }
    public Vector3 Position { get; private set; }

    void Update()
    {

        float curMaxRotation = MaxRotation * Mathf.Lerp(Maneuverability, 1, Mathf.Clamp01(curSpeed / MaxSpeed * 2 - 1));

        curHeading = Mathf.MoveTowardsAngle(curHeading, RequiredHeading, curMaxRotation * Time.deltaTime);
        curSpeed = Mathf.MoveTowards(curSpeed, NeededSpeed, MaxAcceleration);

        transform.rotation = Quaternion.Euler(0, 0, curHeading);
        Velocity = transform.up * curSpeed;
        Position = transform.position = transform.position + Velocity * Time.deltaTime;
    }
    public void CloneHeadingOf(NavigationModule N)
    {
        curHeading = N.curHeading;
        curSpeed = N.curSpeed;
    }
    public void Match(NavigationModule NM)
    {
        RequiredHeading = NM.curHeading;        
        NeededSpeed = Mathf.Clamp(NM.curSpeed, MinSpeed, MaxSpeed);

    }
}
