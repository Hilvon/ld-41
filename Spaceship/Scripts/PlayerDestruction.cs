﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDestruction : DebrisGroup {

    // Use this for initialization
    public event System.Action<float> OnAdjusted;
	float timer=0;

    // Update is called once per frame
    private void OnDestroy()
    {
        if (OnAdjusted != null) OnAdjusted(1);
    }

    void Update () {
        timer += Time.deltaTime;
        if (OnAdjusted != null) OnAdjusted(timer / (1.2f * MaxLS));
	}
}
