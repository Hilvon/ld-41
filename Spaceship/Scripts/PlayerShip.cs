﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(NavigationModule))]
public class PlayerShip : MonoBehaviour, WeaponsSystems.IDamagable {
    public static PlayerShip Instance;
    public static Vector3 Position;
    public static Vector3 Velocity;
	// Use this for initialization
	void Start () {
        Instance = this;
	}

    public CameraTrackOjcect ShipCamera;
    
    NavigationModule _myNM;
    public NavigationModule Navigation {
        get {
            if (_myNM == null) _myNM = GetComponent<NavigationModule>();
            return _myNM;
        }
    }    


    public int MaxWeaponEnergy;
    public float WeaponEnergy;
    public int MaxShieldEnergy;
    public float ShieldEnergy;
    public int MaxThrusterEnergy;
    public float ThrusterEnergy;
    public int MaxMissles;
    public int MissleCost;
    public float Missles;
    public float XP;

    public void AddWeapons(float amount)
    {
        WeaponEnergy += amount;
        if (WeaponEnergy>MaxWeaponEnergy)
        {
            XP += WeaponEnergy - MaxWeaponEnergy;
            WeaponEnergy = MaxWeaponEnergy;
        }
    }
    public void AddShields(float amount)
    {
        ShieldEnergy += amount;
        if (ShieldEnergy > MaxShieldEnergy)
        {
            XP += ShieldEnergy - MaxShieldEnergy;
            ShieldEnergy = MaxShieldEnergy;
        }
    }
    public void AddThruster(float amount)
    {
        ThrusterEnergy += amount;
        if (ThrusterEnergy > MaxThrusterEnergy)
        {
            XP += ThrusterEnergy - MaxThrusterEnergy;
            ThrusterEnergy = MaxThrusterEnergy;
        }
    }
    public void AddMissles(float amount)
    {
        Missles += amount;
        if (Missles > MaxMissles * MissleCost)
        {
            XP += Missles - MaxMissles* MissleCost;
            Missles = MaxMissles;
        }
    }
    public void AddXP(float amount)
    {
        XP += amount;
    }


    void NextTarget() {
        if (Enemy.AllEnemies.Count == 0)
        {
            TargetEnemyIndex = 0;
            Target = null;
        }
        else
        {
            TargetEnemyIndex++;
            while (TargetEnemyIndex >= Enemy.AllEnemies.Count) TargetEnemyIndex -= Enemy.AllEnemies.Count;
            Target = Enemy.AllEnemies[TargetEnemyIndex].transform;
        }
    }

    int TargetEnemyIndex;
    public Transform Target;

    float RequiredHeading;
    float NeededSpeed;

    public float InputSensituvity;   

    public Transform HeadingMarker;
    public Transform TargetMarker;
    public float MaxTargetMarkerDistance;
    float curHeading;

    
    // Update is called once per frame
    void Update()
    {


        NavigationModule N = Navigation;

        Velocity = N.Velocity;
        Position = transform.position;

        RequiredHeading -= Input.GetAxis("Horizontal") * InputSensituvity * 20 * Time.deltaTime;
        NeededSpeed = Mathf.Clamp(NeededSpeed + Input.GetAxis("Vertical") * InputSensituvity * Time.deltaTime, N.MinSpeed, N.MaxSpeed);

        HeadingMarker.rotation = Quaternion.Euler(0, 0, RequiredHeading);
        HeadingMarker.position = transform.position + HeadingMarker.rotation* Vector3.up * NeededSpeed;

        if (Target != null) {
            TargetMarker.position = Target.position;
            TargetMarker.gameObject.SetActive(true);
        } else
        {
            if (Enemy.AllEnemies.Count > 0)
            {
                NextTarget();
                TargetMarker.position = Target.position;
                TargetMarker.gameObject.SetActive(true);
            }
            else
            {
                // Ideally e need to check if we can pick new target as active here
                TargetMarker.gameObject.SetActive(false);
            }
        }

        N.RequiredHeading = RequiredHeading;
        N.NeededSpeed = NeededSpeed;

        if (Input.GetAxis("Jump") > 0)
        {
            NextTarget();
        }
        if (Input.GetAxis("Fire1")>0)
        {
            foreach (FireArkMeshGenerator FA in GetComponentsInChildren<FireArkMeshGenerator>()) FA.Attack();
        }
        if (Input.GetAxis("Fire2") > 0)
        {
            if (Target != null)
            {
                foreach (MissleBay MB in GetComponentsInChildren<MissleBay>()) MB.LaunchMissleAt(Target.transform);
            }
        }
        if (Input.GetAxis("Fire3") > 0)
        {

        }

    }
    public PlayerDestruction DestuctionTemplate;
    public void Die()
    {
        GameManager.SlowTime();
        ShipCamera.ZoomTo(5);
        Instantiate(DestuctionTemplate, transform.position, transform.rotation).OnAdjusted += GameManager.SolidifyGameover;
        gameObject.SetActive(false);
    }
    
    public void ReceiveDamage(float Amount)
    {
        ShieldEnergy -= Amount;
        if (ShieldEnergy <0)
        {
            Field.Instance.ApplyDamage();
            ShieldEnergy = 5;
            Field.Instance.Shaker.CurrentTrauma += Amount;
        }
        else
        {
            Field.Instance.Shaker.CurrentTrauma += Amount / 3;
        }
    }
}
