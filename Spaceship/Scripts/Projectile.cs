﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : WeaponsSystems {
    public float Vel;
    // Use this for initialization
	void Start () {
		
	}
    // Update is called once per frame
	protected override void Update () {
        base.Update();
        transform.position = transform.position + transform.forward * Vel * Time.deltaTime;
	}
}
