﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetMarkerAnumation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
    Vector3 _Min;
    Vector3 _Max;

    public float Period;
    public float Rot;
    public float Max;
    public float Min;
	// Update is called once per frame
	void Update () {
        _Min = Vector3.one * Min;
        _Max = Vector3.one * Max;
        transform.localScale = Vector3.Lerp(_Min, _Max, Mathf.Abs(Mathf.Sin(Time.time * Period)));
        transform.rotation = Quaternion.Euler(0, 0, Time.time*Rot);
	}
}
