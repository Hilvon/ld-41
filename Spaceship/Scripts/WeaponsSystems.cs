﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponsSystems: MonoBehaviour {

    public static float GetHeadingTowards(Vector3 Direction)
    {
        float DirectCourse = Mathf.Acos(Direction.y) * 180 / Mathf.PI;
        if (Direction.x > 0) DirectCourse = 360 - DirectCourse;
        return DirectCourse;
    }

    public static Vector3 FindInterceptPosition(Vector3 MyPos, Vector3 TargetPos, Vector3 TargetVel, float ProjectileVel)
    {
        float PVs = ProjectileVel * ProjectileVel;
        float TVs = TargetVel.sqrMagnitude;
        float TargDist = (MyPos - TargetPos).sqrMagnitude;

        ExtraMathScripts.SquareSolver curSolver = new ExtraMathScripts.SquareSolver((TVs - PVs), 2 * (TargetVel.x * (TargetPos.x - MyPos.x) + TargetVel.y * (TargetPos.y - MyPos.y)), TargDist);

        if (curSolver.IsSOlvable) return TargetPos + TargetVel * ((TVs > PVs) ? curSolver.X1 : curSolver.X2);
        else return TargetPos;
    }

    public interface IDamagable
    {
        void ReceiveDamage(float Amount);
    }

    public float DamageDealt;
    protected virtual void OnTriggerEnter(Collider other)
    {        
        IDamagable Recepient = other.GetComponentInParent<IDamagable>();
        if (Recepient != null) Recepient.ReceiveDamage(DamageDealt);
        //Bake BOOM
        Die();
    }
    float timer;
    public float LifeSpan;

    public GameObject ExplosionPrefab;
    protected void Die()
    {
        Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
    protected virtual void Update()
    {
        timer += Time.deltaTime;
        if (timer >= LifeSpan)
        {
            Die();
            //Bake BOOM
            Destroy(gameObject);
        }
    }

}
