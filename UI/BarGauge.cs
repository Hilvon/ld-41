﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIImage = UnityEngine.UI.Image;
using UIText = UnityEngine.UI.Text;


public class BarGauge : MonoBehaviour {

    public UIImage ControlledImage;
    public UIText ControlledText;
    public void ShowState(float Cur, float Max)
    {
        ControlledText.text = Cur + " / " + Max;
        ControlledImage.fillAmount = Cur / Max;
    }	
}
