﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIText = UnityEngine.UI.Text;

public class GameOverScreen : MonoBehaviour {
    public UIText Score;
    public UIText Time;    
    
    public string MainMenuSceneName;

    public void GoBack()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(MainMenuSceneName);
    }
}
