﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayUI : MonoBehaviour {

    public BarGauge ShieldBar;
    public BarGauge AmmoBar;

    public void DisplayState()
    {
        ShieldBar.ShowState(PlayerShip.Instance.ShieldEnergy, PlayerShip.Instance.MaxShieldEnergy);
        AmmoBar.ShowState(PlayerShip.Instance.WeaponEnergy, PlayerShip.Instance.MaxWeaponEnergy);
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        DisplayState();
    }
}
