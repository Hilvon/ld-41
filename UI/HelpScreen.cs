﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIButon = UnityEngine.UI.Button;
public class HelpScreen : MonoBehaviour {
    public GameObject[] Pages;
    public int Index;

    public UIButon Next;
    public UIButon Prev;
    void MoveToIndex(int I)
    {
        Index = I;
        for (int i = 0; i < Pages.Length; i++)
        {
            Pages[i].SetActive(i == I);
        }
        Prev.interactable = (I > 0);
        Next.interactable = (I < Pages.Length-1);
    }
    public void GoNext()
    {
        MoveToIndex(Index + 1);
    }
    public void GoPrev()
    {
        MoveToIndex(Index - 1);
    }

    // Use this for initialization
    void Start () {
        MoveToIndex(0);

    }

    public string MainMenuSceneName;

    public void GoBack()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(MainMenuSceneName);
    }
}
