﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour {
    public string GameSceneName;
    public string CreditsSceneName;
    public string HowToPlaySceneName;
    public void StartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(GameSceneName);
    }

    public void ShowCredits()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(CreditsSceneName);
    }

    public void ShowHelp()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(HowToPlaySceneName);
    }

    public void Exit()
    {
        Application.Quit();
    }
}
