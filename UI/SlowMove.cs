﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowMove : MonoBehaviour {
    public Vector3 Pos1;
    public Vector3 Pos2;
    public float scrollTime;
    public CanvasGroup Alpha;
    public float Threshold;

    public UnityEngine.Events.UnityEvent OnCompleted;
	// Use this for initialization
	void Start () {
        curTime = 0;
        Time.timeScale = 1;
	}
    float curTime;
	// Update is called once per frame
	void Update () {
        curTime += Time.deltaTime;
        float t = curTime / scrollTime;
        Debug.Log(t);
        transform.position = Vector3.Lerp(Pos1, Pos2, t);
        if (t >= 1)
        {
            Debug.Log("Time to switch scene...");
            OnCompleted.Invoke();
            Destroy(gameObject);
        } else 
        if (Alpha!= null)
        {
            if (t>=Threshold)
            {
                t = 1 - (t - Threshold) / (1 - Threshold);
                Alpha.alpha = t;
            }
        }
        
	}
}
